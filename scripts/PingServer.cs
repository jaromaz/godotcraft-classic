using Godot;
using System;
using System.Threading;
using System.Diagnostics;
using classy.Network;
using classy.Packets;
using classy.Packets.Client;
using classy.Packets.Server;

public class PingServer : Node
{
    [Signal]
    delegate void ServerInfo(string Motd, int Ping);

    public MinecraftConnection Connection;
    public int Ping = -1;
    private Stopwatch _stopwatch;
    private CancellationTokenSource _cancellationTokenSource;
    private CancellationToken _cancellationToken;

    public void RequestServerInfo(string Address, int Port)
    {
        _cancellationTokenSource = new CancellationTokenSource();
        _cancellationToken = _cancellationTokenSource.Token;
        _stopwatch = new Stopwatch();

        // Connect to server
        try
        {
            Connection = new MinecraftConnection(Address, Port, _cancellationToken, ReceivedPacket, ReceivedMapPackets, UpdatePercentage);
        }
        catch (System.Net.Sockets.SocketException)
        {
            return;
        }

        // Request a ServerIdentificationPacket
        IPacket playerIdentificationPacket = new PlayerIdentificationPacket
        {
            ProtocolVersion = 0x07,
            Username = "Ping",
            VerificationKey = "AXY8aCrqVjK43BfH",
        };

        _stopwatch.Start();
        Connection.SendPacket(playerIdentificationPacket);
    }

    public void ReceivedPacket(IPacket packet)
    {
        if (packet is ServerIdentificationPacket serverIdentificationPacket)
        {
            Ping = (int)_stopwatch.ElapsedMilliseconds;
            _stopwatch.Stop();
            EmitSignal("ServerInfo", serverIdentificationPacket.ServerName, serverIdentificationPacket.ServerMotd, Ping);
            TerminateConnection();
        }
        else
        {
            _stopwatch.Stop();
            EmitSignal("ServerInfo", "", "", -1);
            TerminateConnection();
        }
    }

    public void ReceivedMapPackets(byte[] data, LevelFinalizePacket levelFinalizePacket) { }

    public void UpdatePercentage(byte percentage) { }

    public void TerminateConnection()
    {
        _cancellationTokenSource.Cancel();
        if (Connection != null)
        {
            Connection.Close();
        }
    }

    public void Timeout()
    {
        if (Ping == -1)
        {
            _stopwatch.Stop();
            EmitSignal("ServerInfo", "", "", -1);
            TerminateConnection();
        }
    }
}
