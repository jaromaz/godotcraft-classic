extends Control


signal close_escape_menu

var selected_slot = 0
var is_paused

onready var player = get_parent()
onready var hotbar = $VBox/HBox/Hotbar
onready var selector = $VBox/HBox/Hotbar/Selector
onready var escape_menu = $EscapeMenu
onready var crosshair = $Center/Crosshair


func _ready():
	var hotbar_texture = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(0, 0, 182, 22))
	hotbar_texture = Global.get_resized_texture(hotbar_texture, hotbar_texture.get_size().x * 2 * Global.settings["gui_scale"], hotbar_texture.get_size().y * 2 * Global.settings["gui_scale"])
	hotbar.texture = hotbar_texture
	var selector_texture = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(0, 22, 24, 24))
	selector_texture = Global.get_resized_texture(selector_texture, selector_texture.get_size().x * 2 * Global.settings["gui_scale"], selector_texture.get_size().y * 2 * Global.settings["gui_scale"])
	selector.rect_min_size = Vector2(selector_texture.get_size().x, selector_texture.get_size().y)
	selector.texture = selector_texture
	selector.rect_position.y -= 2 * Global.settings["gui_scale"]
	selector.rect_position.x -= 2 * Global.settings["gui_scale"]
	var crosshair_texture = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/widgets.png"), Rect2(243, 3, 9, 9))
	crosshair_texture = Global.get_resized_texture(crosshair_texture, crosshair_texture.get_size().x * 2 * Global.settings["gui_scale"], crosshair_texture.get_size().y * 2 * Global.settings["gui_scale"])
	crosshair.texture = crosshair_texture


func _input(event):
	# Hotbar scrolling
	if Input.is_action_just_pressed("scroll_down"):
		selected_slot += 1
		if selected_slot > 8:
			selected_slot = 0
			selector.rect_position.x -= 8 * 20 * 2 * Global.settings["gui_scale"]
		else:
			selector.rect_position.x += 20 * 2 * Global.settings["gui_scale"]
	if Input.is_action_just_pressed("scroll_up"):
		selected_slot -= 1
		if selected_slot < 0:
			selected_slot = 8
			selector.rect_position.x += 8 * 20 * 2 * Global.settings["gui_scale"]
		else:
			selector.rect_position.x -= 20 * 2 * Global.settings["gui_scale"]
	# Number keys
	if event is InputEventKey and event.scancode >= 49 and event.scancode <= 57:
		selector.rect_position.x = (event.scancode - 49) * 20 * 2 * Global.settings["gui_scale"] - 2 * Global.settings["gui_scale"]
		selected_slot = event.scancode - 49


func toggle_escape_menu(state):
	escape_menu.visible = state


func _on_back_to_game_pressed():
	emit_signal("close_escape_menu")


func _on_disconnect_pressed():
	player.multiplayer_node.terminate_connection()
	Global.try(get_tree().change_scene("res://scenes/multiplayer_menu.tscn"))
	Audio.stop_survival_overworld_music()
	Audio.autoplay_survival_overworld_music()


func _on_Advancements_pressed():
	Global.not_yet_implemented()


func _on_Statistics_pressed():
	Global.not_yet_implemented()


func _on_Options_pressed():
	Global.not_yet_implemented()


func _on_OpenToLan_pressed():
	Global.not_yet_implemented()
