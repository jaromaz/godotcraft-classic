extends Control


onready var bg = $Bg
onready var error_message = $ErrorServer/Message


func _ready():
	# Set up background texture
	var dirt = Global.load_image_texture(Global.texture_dir + "gui/options_background.png")
	bg.texture = Global.get_resized_texture(dirt, dirt.get_size().x * 4 * Global.settings["gui_scale"], dirt.get_size().y * 4 * Global.settings["gui_scale"])
	# Set up error message
	error_message.text = Global.connection_error
	# Show mouse
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _on_back_pressed():
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	Global.try(get_tree().change_scene("res://scenes/multiplayer_menu.tscn"))
