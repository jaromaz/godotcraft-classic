using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Numerics;
using Godot;

namespace classy.Data
{
    public class Map
    {
        public short XSize;
        public short YSize;
        public short ZSize;
        public int Volume;
#nullable enable
        public byte[]? chunksDataCompressed = null;
        public byte[]? chunksData = null;
        public byte[]? blocks = null;
        public (Block, Godot.Vector3)[]? blockData = null;

        public Map() { }

        public void AddChunkData(byte[] chunkData)
        {
            if (chunksDataCompressed == null)
            {
                chunksDataCompressed = chunkData;
            }
            else
            {
                // Add new chunkData to chunksDataCompressed Array
                chunksDataCompressed = Combine(chunksDataCompressed, chunkData);
            }
        }

        public void AddAllChunkData(byte[] chunkData)
        {
            chunksDataCompressed = chunkData;
        }

        public static byte[] Combine(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        public void ProcessLevel(short XSize, short YSize, short ZSize)
        {
            this.XSize = XSize;
            this.YSize = YSize;
            this.ZSize = ZSize;
            Volume = XSize * YSize * ZSize;

            if (chunksDataCompressed != null)
            {
                // Decompress chunk data
                try
                {
                    using (MemoryStream decompressedChunkStream = new MemoryStream())
                    {
                        using (Stream compressedChunkStream = new MemoryStream(chunksDataCompressed))
                        {
                            using (GZipStream gZipStream = new GZipStream(compressedChunkStream, CompressionMode.Decompress))
                            {
                                gZipStream.CopyTo(decompressedChunkStream);
                            }
                        }
                        chunksData = decompressedChunkStream.ToArray();
                    }
                }
                catch
                {
                    GD.Print($"Server did not send valid gzip! Skipping extraction.");
                    chunksData = chunksDataCompressed;
                }
            }

            if (chunksData != null)
            {
                // Process data
                try
                {
                    byte[] blockCountBytes = new byte[4];
                    Buffer.BlockCopy(chunksData, 0, blockCountBytes, 0, 4);
                    int blockCount = BitConverter.ToInt32(blockCountBytes.Reverse().ToArray(), 0);
                    byte[] blockData = chunksData.Skip(4).ToArray();
                    if (blockData.Length != blockCount)
                    {
                        GD.Print($"Error processing chunks: Block count does not match byte array length.");
                        return;
                    }
                    blocks = blockData;
                }
                catch (Exception e)
                {
                    GD.Print($"Error processing chunks: {e.ToString()}");
                }
                blockData = GetBlocks();
            }
        }

        public void ProcessLevel(short XSize, short YSize, short ZSize, byte[] chunksDataCompressed)
        {
            this.chunksDataCompressed = chunksDataCompressed;
            ProcessLevel(XSize, YSize, ZSize);
        }

        public Block? GetBlock(short x, short y, short z)
        {
            if (blocks == null)
            {
                return null;
            }
            int idx = x + XSize * (z + ZSize * y);
            if (idx < blocks.Length && idx >= 0)
                return (Block)blocks[idx];
            else
                return Block.Air;
        }

        public Block? GetBlock(Godot.Vector3 position)
        {
            if (blocks == null)
            {
                return null;
            }
            short x = (short)position.x;
            short y = (short)position.y;
            short z = (short)position.z;
            int idx = x + XSize * (z + ZSize * y);
            if (idx < blocks.Length && idx >= 0)
                return (Block)blocks[idx];
            else
                return Block.Air;
        }

        public void SetBlock(short x, short y, short z, Block block)
        {
            if (blocks == null)
            {
                return;
            }
            int idx = x + XSize * (z + ZSize * y);
            if (idx < blocks.Length && idx >= 0)
                blocks[idx] = (byte)block;
        }

        public void SetBlock(Godot.Vector3 position, Block block)
        {
            if (blocks == null)
            {
                return;
            }
            short x = (short)position.x;
            short y = (short)position.y;
            short z = (short)position.z;
            int idx = x + XSize * (z + ZSize * y);
            if (idx < blocks.Length && idx >= 0)
                blocks[idx] = (byte)block;
        }

        public (Block, Godot.Vector3)[]? GetBlocks()
        {
            if (blocks == null)
            {
                return null;
            }
            (Block, Godot.Vector3)[] blockList = new (Block, Godot.Vector3)[blocks.Length];
            int idx = 0;
            for (int x = 0; x < XSize; x++)
            {
                for (int y = 0; y < YSize; y++)
                {
                    for (int z = 0; z < ZSize; z++)
                    {
                        blockList[idx] = ((Block)blocks[idx], new Godot.Vector3(x, y, z));
                        idx++;
                    }
                }
            }
            return blockList;
        }
#nullable disable
    }
}
