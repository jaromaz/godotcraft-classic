using classy.Network;


namespace classy.Packets.Server
{
    public class PlayerIdentificationPacket : IPacket
    {
        public byte ProtocolVersion { get; set; }
        public string Username { get; set; } = "";
        public string VerificationKey { get; set; } = "";
        public byte Unused = 0x00;

        public void Decode(MinecraftStream minecraftStream)
        {
            ProtocolVersion = minecraftStream.ReadByte();
            Username = minecraftStream.ReadString();
            VerificationKey = minecraftStream.ReadString();
            Unused = minecraftStream.ReadByte();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteByte(ProtocolVersion);
            minecraftStream.WriteString(Username);
            minecraftStream.WriteString(VerificationKey);
            minecraftStream.WriteByte(Unused);
        }
    }
}